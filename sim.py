import sys, os, warnings
import numbers
import time

import numpy as np
import scipy as sp
import sympy as sym
import networkx as nx
import matplotlib as mpl
import matplotlib.pyplot as plt
import numdifftools as nd

from scipy.optimize import minimize
from functools import partial, reduce
from matplotlib import cm

from mna import NumericAnalyzer, SymbolicAnalyzer

formatter = mpl.ticker.EngFormatter()

class Simulator (object):
    def __init__ (self, gnd=f"gnd", abstol=1e-6, optimizer='Nelder-Mead', start_values='smart'):
        self.gnd = gnd
        self.abstol = abstol
        self.optimizer = optimizer
        self.sym_analyzer = SymbolicAnalyzer()
        self.num_analyzer = NumericAnalyzer()

        self.start_values = start_values

        self.dc_graph = nx.MultiDiGraph()
        self.ss_cache = {}
        self.x0 = None

    def plot_dc_graph (self, node_cm='autumn', edge_cm='winter'):
        pos = nx.shell_layout(self.dc_graph)
        pot = nx.get_node_attributes(self.dc_graph, 'potential')
        cur = [ d['i'] for _,_,d in self.dc_graph.edges(data=True)]
        nx.draw( self.dc_graph
               , pos
               , labels={ n:f"{n}: {formatter(p)}V"
                          for n,p in pot.items() }
               , edge_color=cur
               , edge_cmap=cm.get_cmap(edge_cm, len(cur))
               , width=1
               , linewidths=1
               , node_color=list(pot.values())
               , node_cmap=cm.get_cmap(node_cm, len(pot))
               , node_size=2000
               , connectionstyle="arc3,rad=-0.3"
               , alpha=0.75 )
        nx.draw_networkx_edge_labels( self.dc_graph
                                    , pos
                                    , edge_labels={ (u,v): f"{d['symbol'].name}: {formatter(d['i'])}A"
                                                    for u,v,d in self.dc_graph.edges(data=True) })
        plt.axis('off')
        return plt.show()

    def build_dc_graph (self, devices):
        self.dc_graph = nx.MultiDiGraph()

        for name,device in devices.items():
            if device['component'] == 'V':
                m = f"φ{device['pins']['+']}"
                p = f"φ{device['pins']['-']}"
                f = None
                c = None
                pp = sym.Symbol(p)
                pm = device['value']
            elif device['component'] == 'VCVS':
                m = f"φ{device['pins']['+']}"
                p = f"φ{device['pins']['-']}"
                f = None
                c = f"φ{device['controller']}"
                pp = sym.Symbol(p)
                pm = sym.Symbol(m)
            elif device['type'] == 'active':
                p = f"φ{device['pins']['D']}" if device['component'] == 'nmos'\
                                              else f"φ{device['pins']['S']}"
                m = f"φ{device['pins']['S']}" if device['component'] == 'nmos'\
                                              else f"φ{device['pins']['D']}"
                f = partial(device['opps'], device['w'], device['l'])
                c = None
                pp = sym.Symbol(p)# if device['component'] == 'nmos' else m)
                pm = sym.Symbol(m)# if device['component'] == 'nmos' else p)
            else:
                p = f"φ{device['pins']['+']}"
                m = f"φ{device['pins']['-']}"
                f = device['idc']
                c = None
                pp = sym.Symbol(p)
                pm = sym.Symbol(m)

            if not self.dc_graph.has_node(p):
                self.dc_graph.add_node(p, potential=pp, controller=c)
            if not self.dc_graph.has_node(m):
                self.dc_graph.add_node(m, potential=pm, controller=c)

            kcl_name = f"I_{name}"

            self.dc_graph.add_edge( p, m
                                  , key=name
                                  , dc_current=f
                                  , symbol=sym.Symbol(kcl_name)
                                  , i=0.0 )

        self.dc_graph.nodes[f"φ{self.gnd}"]['potential'] = 0.0
        return self.dc_graph

    def calculate_starting_point (self, uk_v, uk_i, devices):
        #potentials = nx.get_node_attributes(self.dc_graph, 'potential')
        #k_v = {k: v for k,v in potentials.items() if k not in uk_v }
        k_v = { n: d['potential']
                for n,d in self.dc_graph.nodes(data=True)
                if n not in uk_v
                   and d['controller'] is None }

        φdd = max(k_v, key=k_v.get)
        vdd = k_v[φdd]
        φss = min(k_v, key=k_v.get)
        vss = k_v[φss]

        for φn in uk_v:
            up = nx.shortest_path_length( self.dc_graph
                                        , source=φn
                                        , target=φdd )
            dn = nx.shortest_path_length( self.dc_graph
                                        , source=φn
                                        , target=φss )
            vn = (vdd / (up + dn)) * dn
            self.dc_graph.nodes[φn]['potential'] = vn

            for n,d in self.dc_graph.nodes(data=True):
                if d['controller'] == φn:
                    self.dc_graph.nodes[n]['potential'] = vn

        Φ = { n: d['potential'] for n,d in self.dc_graph.nodes(data=True) }

        loops = list(nx.simple_cycles(self.dc_graph))
        kvl = lambda c: [ [ (u,v,k) for u,v,k in self.dc_graph.edges(keys=True)
                                    if u == uu and v == vv ][0]
                         for uu,vv in zip(c, (c[1:] + [c[0]]))]

        for u,v,k,d in self.dc_graph.edges(keys=True, data=True):
            if d['symbol'].name not in uk_i:
                continue

            ib = []
            src = list(filter(lambda x: x != f"φ{self.gnd}", [u, v]))
            simple = list(filter(lambda l: src[0] in l, loops))[0]
            cycle = kvl(simple)
            #cycle = nx.find_cycle(self.dc_graph, source=src)

            for cu,cv,ck in cycle:
                if ck == k:
                    continue

                cc = self.dc_graph[cu][cv][ck]['dc_current']\
                        (*[Φ[f"φ{pin}"] for pin in devices[ck]['pins'].values()])
                ib += [cc if devices[ck]['type'] != 'active' else cc['id']]

            self.dc_graph[u][v][k]['i'] = sum(ib) / len(ib)

        x0 = [ v['potential'] for k,v in self.dc_graph.nodes(data=True)
                              if k in uk_v ] \
           + [ d['i'] for _,_,d in self.dc_graph.edges(data=True)
                      if d['symbol'].name in uk_i ]
        return x0

    def find_operating_point (self, devices):
        G = self.build_dc_graph(devices)

        for node in self.dc_graph.nodes:
            try:
               cycle = nx.find_cycle(self.dc_graph, source=node)
            except:
                edges = list(self.dc_graph.in_edges(node, keys=True)) \
                      + list(self.dc_graph.out_edges(node, keys=True))
                for u,v,k in edges:
                    self.dc_graph[u][v][k]['dc_current'] = lambda φp,φm: 0.0
                pass

        unknown_currents = list(set([ d['symbol'].name
                                      for _,_,d in self.dc_graph.edges(data=True)
                                      if d['dc_current'] is None ]))

        unknown_voltages = list(set([ d['potential'].name
                                      for _,d in self.dc_graph.nodes(data=True)
                                      if not isinstance(d['potential'], numbers.Number)
                                         and d['controller'] is None ]))

        unknowns = unknown_voltages + unknown_currents

        def f (x):
            uk_dict = dict(zip(unknowns, x))

            for key,value in uk_dict.items():
                if self.dc_graph.has_node(key):
                    self.dc_graph.nodes[key]['potential'] = value
                for n,d in self.dc_graph.nodes(data=True):
                    if d['controller'] == key:
                        d['potential'] = value

            potentials = { n: d['potential'] for n,d in self.dc_graph.nodes(data=True) }

            for u,v,k,d in self.dc_graph.edges(keys=True, data=True):
                Φ = [potentials[f"φ{pin}"] for pin in devices[k]['pins'].values()]

                if d['symbol'].name in uk_dict.keys():
                    self.dc_graph[u][v][k]['i'] = uk_dict[d['symbol'].name]
                else:
                    res = self.dc_graph[u][v][k]['dc_current'](*Φ)
                    self.dc_graph[u][v][k]['i'] = res['id'] if type(res) == dict else res

            kcl_error = np.sum(np.array([ np.abs( self.dc_graph.out_degree(n, weight='i')
                                                - self.dc_graph.in_degree(n, weight='i') )
                                          for n in self.dc_graph.nodes
                                          if n in uk_dict.keys()]))
            return kcl_error

        #self.x0 = np.random.rand(len(unknown_currents) + len(unknown_voltages))
        x0 = self.x0 if self.x0 is not None else \
             self.calculate_starting_point( unknown_voltages
                                          , unknown_currents
                                          , devices )

        x_ = minimize( f, x0
                     , method=self.optimizer
                     , tol=self.abstol)


        if x_.success:
            self.x0 = x_.x
        else:
            warnings.warn("No Solution Found, reset x0 and try again.", RuntimeWarning)
            return None

        node_potentials = { node: data['potential']
                            for node,data in self.dc_graph.nodes(data=True)}
        branch_currents = { d['symbol'].name: d['i']
                            for u,v,d in self.dc_graph.edges(data=True) }


        return { **node_potentials, ** branch_currents }

    def nodal_analyzer (self, devices, values, s=0.0):
        self.num_analyzer = NumericAnalyzer(gnd=self.gnd)

        grounded = { name: vsrc['pins']['+'] for name,vsrc in devices.items()
                                             if vsrc['component'] == 'V'
                                             and vsrc['acm'] is None
                                             and vsrc['acp'] is None }

        opened = { name: isrc['pins'] for name,isrc in devices.items()
                                      if isrc['component'] == 'I'
                                      and isrc['amp'] is None
                                      and isrc['frq'] is None }

        for name,device in devices.items():
            if name in grounded.keys() \
            or name in opened.keys():
                continue

            nodes = { pin: self.gnd if node in grounded.values() else node
                      for pin,node in device['pins'].items() }

            if device['type'] == 'active':
                g,d,s,b = list(map(nodes.get, ['G', 'D', 'S', 'B']))

                self.num_analyzer.add('VCCS', [g, s, d, s], f"gm_{name}", values[f"gm_{name}"])
                self.num_analyzer.add('VCCS', [b, s, d, s], f"gmb_{name}", values[f"gmb_{name}"])

                self.num_analyzer.add('G', [d, s], f"gds_{name}", values[f"gds_{name}"])

                self.num_analyzer.add('C', [g, d], f"cgd_{name}", values[f"cgd_{name}"])
                self.num_analyzer.add('C', [g, b], f"cgb_{name}", values[f"cgb_{name}"])
                self.num_analyzer.add('C', [g, s], f"cgs_{name}", values[f"cgs_{name}"])
                self.num_analyzer.add('C', [d, s], f"cds_{name}", values[f"cds_{name}"])
                self.num_analyzer.add('C', [s, b], f"csb_{name}", values[f"csb_{name}"])
                self.num_analyzer.add('C', [d, b], f"cdb_{name}", values[f"cdb_{name}"])
            else:
                component = device['component']
                p,m = [ self.gnd if n in grounded.values() else n
                        for n in list(device['pins'].values()) ]
                self.num_analyzer.add(component, [p, m], name , values[name])

        return self.num_analyzer.analyze

    def dc_operating_point (self, devices):
        dcop = self.find_operating_point(devices)

        if dcop is None:
            warnings.warn("No Operating Point Found.", RuntimeWarning)
            return None

        actives = dict(reduce( lambda a,b: dict(a, **b)
                             , [ { f"{opp}_{name}": val
                                   for opp,val
                                   in mos['opps']( mos['w'], mos['l']
                                                 , *[ dcop[f"φ{pin}"]
                                                      for pin
                                                      in mos['pins'].values() ]).items() }
                                 for name,mos
                                 in devices.items()
                                 if mos['type'] == 'active' ] ))

        passives = { name: data['value']
                     for name,data in devices.items()
                     if data['type'] != 'active' }

        return {**actives, **passives}

    def symbolic_analysis (self, devices):
        self.sym_analyzer = SymbolicAnalyzer()

        grounded = { name: vsrc['pins']['+'] for name,vsrc in devices.items()
                                             if vsrc['component'] == 'V'
                                             and vsrc['acm'] is None
                                             and vsrc['acp'] is None }

        for name,device in devices.items():
            if name in grounded.keys():
                continue

            nodes = { pin: self.gnd if node in grounded.values() else node
                      for pin,node in device['pins'].items() }

            if device['type'] == 'active':
                g,d,s,b = list(map(nodes.get, ['G', 'D', 'S', 'B']))

                self.sym_analyzer.add('VCCS', [g, s, d, s], f"gm_{name}")
                self.sym_analyzer.add('VCCS', [b, s, d, s], f"gmb_{name}")

                self.sym_analyzer.add('G', [d, s], f"gds_{name}")

                self.sym_analyzer.add('C', [g, d], f"cgd_{name}")
                self.sym_analyzer.add('C', [g, b], f"cgb_{name}")
                self.sym_analyzer.add('C', [g, s], f"cgs_{name}")
                self.sym_analyzer.add('C', [d, s], f"cds_{name}")
                self.sym_analyzer.add('C', [s, b], f"csb_{name}")
                self.sym_analyzer.add('C', [d, b], f"cdb_{name}")
            else:
                component = device['component']
                p,m = [ self.gnd if n in grounded.values() else n
                        for n in list(device['pins'].values()) ]
                self.sym_analyzer.add(component, [p, m], name)

        return self.sym_analyzer.analyze

    def simulate_symbolic (self, devices):
        t0 = time.time()
        print(f"Arrange Symbolic Nodal Analysis Equations ... ", end='')
        V,I = self.symbolic_analysis(devices)
        t1 = time.time()
        print(f"Took {formatter(t1 - t0)}s.")

        if V is None or I is None:
            warnings.warn("Symbolic Analysis Failed!", RuntimeWarning)
            return None

        print(f"Performing Numeric Analisys to find Node Voltages ... ", end='')
        A,P = self.dc_analysis(devices)
        t2 = time.time()
        print(f"Took {formatter(t2 - t1)}s.")

        if A is None or P is None:
            warnings.warn("Numeric Analysis Failed!", RuntimeWarning)
            return None

        equations = {**V, **I}
        values = {**A, **P}

        print(f"Lambdifying Symbolic Expressions for convenience ... ", end='')
        result = { res: eq.subs({ symbol.name: values[symbol.name]
                                  for symbol in eq.free_symbols
                                  if symbol.name != 's' })
                   for res,eq in equations.items() }

        self.ss_cache = { par: sym.lambdify([sym.Symbol('s')], eq, 'numpy') \
                               if eq.free_symbols else float(eq)
                          for par,eq in result.items() }
        t3 = time.time()
        print(f"Took {formatter(t3 - t2)}s.")

        print(f"Results can be obtained from the DC-Graph and the Small-Signal-Cache.")
        print(f"Numeric Simulation Approximation took {t3 - t0}s")

        return self.ss_cache
