(import [pandas :as pd])
(import [numpy :as np])
(import [networkx :as nx])
(import [scipy :as sp])
(import [scipy.optimize [minimize]])
(import [sklearn.utils.extmath [cartesian]])
(import [matplotlib :as mpl])
(import [matplotlib.pyplot :as plt])
(import time numbers)
(import warnings)
(import [multiprocessing [Process Pool set_start_method]])

(import [sim [Simulator]] 
        [mna [NumericAnalyzer]] 
        [scm [Schematic]]
        [dev [PrimitiveDevice]])

(require [hy.contrib.walk [let]])
(require [hy.contrib.loop [loop]])

(setv eng (.EngFormatter mpl.ticker))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Current Mirror
;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn CurrentMirror [wi wo l iin iout &kwonly [typ "n"] [vdd 1.2]]
    (let [cm (Schematic :gnd "SS")]
        (.add-v cm vdd "DD" "SS" :name "Vdd")
        (if (= typ "n")
            (do (.add-i cm iin "DD" "I" :name "Iin")
                ;(.add-i cm iout "DD" "O" :name "Iout")
                (.add-vcvs cm "I" "O" "SS" :name "Vx")
                (.add-nmos-wl cm "I" "I" "SS" "SS" wi l
                                 "./models/gpdk090_nmos1v/nmos1v"
                                 :name "NMin")
                (.add-nmos-wl cm "I" "O" "SS" "SS" wo l
                                 "./models/gpdk090_nmos1v/nmos1v"
                                 :name "NMout"))
            (do (.add-i cm iin "I" "SS" :name "Iin")
                ;(.add-i cm iout "SS" "O" :name "Iout")
                (.add-vcvs cm "I" "O" "SS" :name "Vx")
                (.add-pmos-wl cm "I" "I" "DD" "DD" wi l
                                 "./models/gpdk090_pmos1v/pmos1v"
                                 :name "PMin")
                (.add-pmos-wl cm "I" "O" "DD" "DD" wo l
                                 "./models/gpdk090_pmos1v/pmos1v"
                                 :name "PMout")))
        cm))

(setv sim (Simulator :optimizer "Nelder-Mead" :abstol 1e-15 :gnd "SS"))
(setv scm (CurrentMirror 2e-6 4e-6 3e-7 5e-6 5e-6 :typ "p"))
(setv opp (.dc-operating-point sim scm.devices))

(defn ncm-col [inp]
    (let [num (first inp)
          dps (second inp)
          w 2e-6
          l (get dps 0)
          i (get dps 1)
          M (.limit-denominator (fraction (get dps 2)) 10)
          wi (* w (. M numerator))
          wo (* w (. M denominator))
          iin (* i (. M numerator))
          iout (* i (. M denominator))
          sim (Simulator :optimizer "Nelder-Mead" 
                         :abstol 1e-15
                         :gnd "SS")
          scm (CurrentMirror wi wo l iin iout :typ "p")
          opp (.dc-operating-point sim scm.devices)]
        (if opp
            (let [Vi (get (.get-node-attributes nx sim.dc-graph
                                                "potential") 
                          "φI")
                  Vo (get (.get-node-attributes nx sim.dc-graph
                                                "potential") 
                          "φO")
                  ii (get opp "id_NMin")
                  io (get opp "id_NMout")
                  Vovd (- Vi (get opp "vth_NMout"))
                  Vdsat (get opp "vdsat_NMout")
                  Ro (/ 1 (get opp "gds_NMout"))
                  Ri (/ 1 (get opp "gm_NMin"))]
                (print (.format (+ "#{0}: L = {10}m, I = {11}A,  M = {1}\n"
                                   "∆Iin = {2}A = {3}A - {4}A = {5:.2f}%\n"
                                   "∆Iout = {6}A = {7}A - {8}A = {9:.2f}%")
                                   num M 
                                   (eng (- iin ii)) (eng iin) (eng ii)
                                   (* (/ (abs (- iin ii)) iin) 100)
                                   (eng (- iout io)) (eng iout) (eng io)
                                   (* (/ (abs (- iout io)) iout) 100)
                                   (eng l)
                                   (eng i)))
                (if (and (<= (/ (abs (- iin ii)) iin) 0.5)
                         (<= (/ (abs (- iout io)) iout) 0.5))
                    (.array np [l wi wo (float M) Vovd Vdsat ii io Ri Ro])
                    (* (.ones np 10) np.nan)))
            (* (.ones np 10) np.nan))))

(defn generate-ncm [&kwonly [agents 10] 
                            [chunksize 10]
                            [base-width 2e-6]]
    (let [L (.linspace np 3e-7 3e-6 :num 10) ; [3e-7] 
          M (.unique np 
                (list (map 
                    (fn [x] (fraction (first x) (second x))) 
                    (cartesian [(.arange np 1 11) 
                                (.arange np 1 11)]))))
          I (* (.arange np 1 11) 1e-6) ; [15e-6] 
          cols ["l" "wi" "wo" "M" "Vovd" "Vdsat" "Ii" "Io" "Ri" "Ro"]
          input-data (cartesian [L I M])
          data (.vstack np
            (with [pool (Pool :processes agents)]
                    (.map pool ncm-col
                                (enumerate input-data)
                                chunksize)))
          output-data (get data (.any (~ (.isnan np data)) :axis 1))]
        ;(.array np (list (map ncm-col (enumerate input-data))))
        (print (.format "In: {0}, Out: {1}" (len input-data) (. output-data shape)))
        output-data))

(setv cols ["l" "wi" "wo" "M" "Vovd" "Vdsat" "Ii" "Io" "Ri" "Ro"])
(defn frame-and-save [data-base data-name &kwonly [columns cols]]
    (let [df (.DataFrame pd :data data-base :columns columns)]
        (.to-hdf (.dropna (.drop-duplicates df))
                 (+ "./data/" data-name ".h5")
                 :key "database" 
                 :mode "w")
        df))

(with [(.catch-warnings warnings)]
    (.simplefilter warnings "ignore")
    (setv data (generate-ncm)))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Differential Pair
;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn DifferentialPair [w l iref &kwonly [typ "n"] [vdd 1.2]]
    (let [dp (Schematic :gnd "SS")]
        (.add-v dp (vdd "DD" "SS" :name "Vdd"))
        (.add-v dp ((/ vdd 2) "I1" "SS" :name "Vi1") :acm (+ 2e-3 (/ vid 2)))
        (.add-v dp ((/ vdd 2) "I2" "SS" :name "Vi2") :acm (- 2e-3 (/ vid 2)))
        (if (= typ "n")
            (do (.add-i dp (/ iref 2) "DD" "O1" :name "Id1")
                (.add-i dp (/ iref 2) "DD" "O2" :name "Id2")
                (.add-i dp iref "X" "SS" :name "Iref")
                (.add-nmos-wl dp "IN" "O1" "X" "X" w l
                                 "./models/gpdk090_nmos1v/nmos1v"
                                 :name "NM1")
                (.add-nmos-wl dp "IP" "O2" "X" "X" w l
                                 "./models/gpdk090_nmos1v/nmos1v"
                                 :name "NM2"))
            (do (.add-i dp (/ iref 2) "O1" "SS" :name "Idn")
                (.add-i dp (/ iref 2) "O2" "SS" :name "Idp")
                (.add-i dp iref "DD" "X" :name "Iref")
                (.add-pmos-wl dp "IN" "O1" "X" "X" w l
                                 "./models/gpdk090_pmos1v/pmos1v"
                                 :name "PMN")
                (.add-pmos-wl dp "IP" "O2" "X" "X" w l
                                 "./models/gpdk090_pmos1v/pmos1v"
                                 :name "PMP")))
        dp))

(defn ndp-col [dps]
    (let [w (get dps 0)
          l (get dps 1)
          iref (get dps 2)
          sim (Simulator :optimizer "Nelder-Mead" 
                         :abstol 1e-13 
                         :gnd "SS")
          scm (DifferentialPair w l iref :typ "n")
          dev scm.devices
          opp (.dc-operating-point sim dev)]
        (if opp
            (let [vi (get (.get-node-attributes nx sim.dc-graph
                                                "potential") 
                          "φI")
                  vo (get (.get-node-attributes nx sim.dc-graph
                                                "potential") 
                          "φO")
                  id1 (get opp "id_NM1")
                  id2 (get opp "id_NM2")
                  an (.nodal-analyzer sim dev opp)

                  jw (np.linspace 0 1e10 :num (int 1e3))
                  ops (lfor s jw (an :jw s))
                  vo1 (.array np (lfor op ops (get op "V_O1")))
                  vo2 (.array np (lfor op ops (get op "V_O2")))
                  vi1 (.array np (lfor op ops (get op "V_I1")))
                  vi2 (.array np (lfor op ops (get op "V_I2")))
                  Avd (/ (- vo1 vo2) (- vi1 vi2))
                  f3dB (get jw (.argmax np (<= Av (- (first Avd) 3))))
                  gds (get opp "gsd_NMout")]
                [w l iref gds f3dB])
            (* (.ones np 12) np.nan))))

(defn generate-ndp [&kwonly [num-samples 20] 
                            [agents 20] 
                            [chunksize 1000]]
    (let [W (.linspace np 2e-6 10e-6 :num 20)
          L (.linspace np 3e-7 3e-6 :num 20)
          Iref (.linspace np 1e-6 50e-6 :num 25)
          cols ["w" "l" "Iref" "Vic" "Vid" "CMRR" 
                "Avd" "Acm" "f3dB" "Vcmin" "Vcmanx"]
          input-data (cartesian [W L Iref])]
        (print (.format "Generating {0} samples" (len input-data)))
        (with [pool (Pool :processes agents)]
                (.array np 
                    (list (.map pool ndp-col
                                input-data 
                                chunksize))))))

(defn frame-and-save [data-base data-name &kwonly [columns None]]
    (let [df (.DataFrame pd :data data-base :columns columns)]
        (.to-hdf (.dropna (.drop-duplicates df))
                 (+ "./data/" data-name ".h5")
                 :key "database" 
                 :mode "w")))

(with [(.catch-warnings warnings)]
    (.simplefilter warnings "ignore")
    (setv data (generate-ndp)))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common Source
;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn CommonSource [w l vidc viac cl iref &kwonly [typ "n"] [vdd 1.2]]
    (let [cs (Schematic :gnd "SS")]
        (.add-v cs vdd "DD" "SS" :name "Vdd")
        (.add-v cs vidc "I" "SS" :acm viac :name "Vin")
        (.add-c cs cl "O" "SS" :name "CL")
        (if (= typ "n")
            (do (.add-i cs iref "DD" "O" :name "Iref")
                (.add-nmos-wl cs "I" "O" "SS" "SS" w l 
                                 "./models/gpdk090_nmos1v/nmos1v" 
                                 :name "NM1"))
            (do (.add-i cs iref "O" "SS" :name "Iref")
                (.add-pmos-wl cs "I" "O" "DD" "DD" w l
                                 "./models/gpdk090_pmos1v/pmos1v"
                                 :name "PM1")))
        cs))

(defn CommonGate [w l vidc viac cl iref &kwonly [typ "n"] [vdd 1.2]]
    (let [cg (Schematic :gnd "SS")]
        (.add-v cg vdd "DD" "SS" :name "Vdd")
        (.add-v cg vidc "I" "SS" :acm viac :name "Vin")
        (.add-c cg cl "O" "SS" :name "CL")
        (if (= typ "n")
            (do (.add-i cg iref "DD" "O" :name "Iref")
                (.add-nmos-wl cg "SS" "O" "I" "I" w l 
                                 "./models/gpdk090_nmos1v/nmos1v" 
                                 :name "NM1"))
            (do (.add-i cg iref "O" "SS" :name "Iref")
                (.add-pmos-wl cg "SS" "O" "I" "I" w l
                                 "./models/gpdk090_pmos1v/pmos1v"
                                 :name "PM1")))
        cg))

(defn CommonDrain [w l vidc viac cl iref &kwonly [typ "n"] [vdd 1.2]]
    (let [cd (Schematic :gnd "SS")]
        (.add-v cd vdd "DD" "SS" :name "Vdd")
        (.add-v cd vidc "I" "SS" :acm viac :name "Vin")
        (.add-c cd cl "O" "SS" :name "CL")
        (if (= typ "n")
            (do (.add-i cd iref "O" "SS" :name "Iref")
                (.add-nmos-wl cd "I" "DD" "O" "I" w l 
                                 "./models/gpdk090_nmos1v/nmos1v" 
                                 :name "NM1"))
            (do (.add-i cd iref "DD" "O" :name "Iref")
                (.add-pmos-wl cd "I" "SS" "O" "O" w l
                                 "./models/gpdk090_pmos1v/pmos1v"
                                 :name "PM1")))
        cd))


;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constant Current Source
;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn nccs-col [dps]
    (let [w (get dps 0)
          l (get dps 1)
          vg (get dps 2)
          vs 0.0
          vb 0.0
          vd 0.6
          model (with [(.catch-warnings warnings)]
                    (.simplefilter warnings "ignore")
                    (PrimitiveDevice "./models/gpdk090_nmos1v/nmos1v"))
          opp (.predict model w l vd vg vs vb)
          i (get opp "id")
          gds (get opp "gds")]
        [w l vg i gds]))

(defn CurrentSource [model params num-samples
                     &kwonly [agents 20] [chunksize 250]]
    (let [vd 0.6 vs 0.0 vb 0.0
          W (.linspace np 1e-6 10e-6 :num num-samples)
          L (.linspace np 300e-9 2e-6 :num num-samples)
          VG (.linspace np 0 1.2 :num num-samples)
          data (.array np (list (map nccs-col 
                                     (cartesian [W L VG]))))]
        (.DataFrame pd :data data :columns ["w" "l" "vgs" "id" "gds"])))

(defn generate-nccs []
    (let [nmos1v (PrimitiveDevice "./models/gpdk090_nmos1v/nmos1v")
          cs-params ["id" "gds"]
          num-samples 50
          ncs-df (CurrentSource nmos1v cs-params num-samples)]
        (.to-hdf (.drop-duplicates ncs-df) 
                 "./data/nmos1v-ccs.h5" 
                 :key "database" 
                 :mode "w")
        ncs-df))

