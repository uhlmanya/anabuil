import scipy as sp
from scipy.optimize import minimize
import numpy as np
import sympy as sym
import networkx as nx
import matplotlib as mpl
import matplotlib.pyplot as plt

import time
import numbers

from mna import NumericAnalyzer, SymbolicAnalyzer
from sim import Simulator
from scm import Schematic

eng = mpl.ticker.EngFormatter()

def CommonSource (w, l, vidc, viac, cl, iref, typ='n', vdd=1.2):
    cs = Schematic()

    cs.add_v(vdd, "DD", "gnd", name="Vdd")
    cs.add_v(vidc, "I", "gnd", acm=viac, name="Vin")
    cs.add_c(cl, "O", "gnd", name="CL")

    if typ == 'n':
        cs.add_i(iref, "DD", "O", name="Iref" )
        cs.add_nmos_wl( "I", "O", "gnd", "gnd"
                      , w, l
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='NM1' )
    else:
        cs.add_i(iref, "O", "gnd", name="Iref" )
        cs.add_pmos_wl( "I", "O", "DD", "DD"
                      , w, l
                      , "./models/gpdk090_pmos1v/pmos1v"
                      , name='PM1' )

    return cs

def CommonSourceR (w, l, vidc, viac, cl, rref, typ='n', vdd=1.2):
    cs = Schematic()

    cs.add_v(vdd, "DD", "gnd", name="Vdd")
    cs.add_v(vidc, "I", "gnd", acm=viac, name="Vin")
    cs.add_c(cl, "O", "gnd", name="CL")

    if typ == 'n':
        cs.add_r(rref, "DD", "O", name="Rref" )
        cs.add_nmos_wl( "I", "O", "gnd", "gnd"
                      , w, l
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='NM1' )
    else:
        cs.add_r(rref, "O", "gnd", name="Rref" )
        cs.add_pmos_wl( "I", "O", "DD", "DD"
                      , w, l
                      , "./models/gpdk090_pmos1v/pmos1v"
                      , name='PM1' )

    return cs

def CommonGate (w, l, vidc, viac, cl, iref, typ='n', vdd=1.2):
    cg = Schematic()

    cg.add_v(vdd, "DD", "gnd", name="Vdd")
    cg.add_v(vidc, "I", "gnd", acm=viac, name="Vin")
    cg.add_c(cl, "O", "gnd", name="CL")

    if typ == 'n':
        cg.add_i(iref, "DD", "O", name="Iref" )
        cg.add_nmos_wl( "gnd", "O", "I", "I"
                      , w, l
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='NM1' )
    else:
        cg.add_i(iref, "O", "gnd", name="Iref" )
        cg.add_pmos_wl( "gnd", "O", "I", "I"
                      , w, l
                      , "./models/gpdk090_pmos1v/pmos1v"
                      , name='PM1' )
    return cg

def CommonDrain (w, l, vidc, viac, cl, iref, typ='n', vdd=1.2):
    cd = Schematic()

    cd.add_v(vdd, "DD", "gnd", name="Vdd")
    cd.add_v(vidc, "I", "gnd", acm=viac, name="Vin")
    cd.add_c(cl, "O", "gnd", name="CL")

    if typ == 'n':
        cs.add_i(iref, "O", "gnd", name="Iref" )
        cs.add_nmos_wl( "I", "DD", "O", "O"
                      , w, l
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='NM1' )
    else:
        cs.add_i(iref, "DD", "O", name="Iref" )
        cs.add_pmos_wl( "I", "gnd", "O", "O"
                      , w, l
                      , "./models/gpdk090_pmos1v/pmos1v"
                      , name='PM1' )

    return cd

def CurrentMirror (w1, l1, w2, l2, iin, iout, typ='n', vdd=1.2):
    cm = Schematic()

    cm.add_v(vdd, "DD", "gnd", name="Vdd")

    if typ == 'n':
        cs.add_i(iin, "DD", "I", name="Iin" )
        cs.add_i(iout, "DD", "O", name="Iout" )
        cs.add_nmos_wl( "I", "I", "gnd", "gnd"
                      , w1, l1
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='NM1' )
        cs.add_nmos_wl( "I", "O", "gnd", "gnd"
                      , w2, l2
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='NM2' )
    else:
        cs.add_i(iin, "gnd", "I", name="Iin" )
        cs.add_i(iout, "gnd", "O", name="Iout" )
        cs.add_pmos_wl( "I", "I", "DD", "DD"
                      , w1, l1
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='PM1' )
        cs.add_pmos_wl( "I", "O", "DD", "DD"
                      , w2, l2
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='PM2' )

    return cm

def DifferentialPair ( w1, l1, w2, l2, vip, vin, viac, iref, idp, idn, cl
                     , typ='n', vdd=1.2):
    dp = Schematic()

    dp.add_v(vdd, "DD", "gnd", name="Vdd")
    dp.add_v(vip, "IP", "gnd", acm=viac, name="Vip")
    dp.add_v(vin, "IN", "gnd", acm=viac, name="Vin")

    if typ == 'n':
        dp.add_i(idn, "DD", "ON", name="Idn" )
        dp.add_i(idp, "DD", "OP", name="Idp" )
        dp.add_i(iref, "X", "gnd", name="Iref" )
        dp.add_nmos_wl( "IN", "ON", "X", "X"
                      , w1, l1
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='NM1' )
        dp.add_nmos_wl( "IP", "OP", "X", "X"
                      , w2, l2
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='NM2' )
    else:
        dp.add_i(idn, "ON", "gnd", name="Idn" )
        dp.add_i(idp, "OP", "gnd", name="Idp" )
        dp.add_i(iref, "DD", "X", name="Iref" )
        dp.add_pmos_wl( "IN", "ON", "X", "X"
                      , w1, l1
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='PM1' )
        dp.add_pmos_wl( "IP", "OP", "X", "X"
                      , w2, l2
                      , "./models/gpdk090_nmos1v/nmos1v"
                      , name='PM2' )

    return dp

def generator (scm):
    #CommonSource (w, l, vidc, viac, cl, iref):
    scm = CommonSource (3.25e-6, 300e-9, 0.3, 0.05, 1e-12, 20e-6)
    scm = CommonSourceR (3.25e-6, 300e-9, 0.3, 0.05, 1e-12, 10e3)

    sim = Simulator(optimizer='Nelder-Mead', abstol=1e-13)

    op = sim.dc_operating_point(scm.devices)
    analyzer = sim.nodal_analyzer(scm.devices, op)

    res = analyzer(jw=0)
    gain = 20 * np.log10(np.abs(res['V_O'] / res['V_I']))

    jw = np.linspace(0, 1e9, num=int(1e5))
    vo = np.array([ analyzer(jw=s)['V_O'] for s in jw ])
    vi = np.array([ analyzer(jw=s)['V_I'] for s in jw ])
    Av = 20 * np.log10(np.abs(vo/vi))

    f3dB = jw[np.argmax(Av <= (gain - 3))]
    UGBW = jw[np.argmax(Av <= 0)]

    plt.semilogx(jw, Av)
    plt.axhline(y=gain, color='b', linestyle='dashed', linewidth=1)
    plt.axvline(x=f3dB, color='r', linestyle='dashed', linewidth=1)
    plt.axhline(y=(gain - 3), color='r', linestyle='dashed', linewidth=1)
    plt.axvline(x=UGBW, color='g', linestyle='dashed', linewidth=1)
    plt.axhline(y=0, color='g', linestyle='dashed', linewidth=1)
    plt.grid()
    plt.xlim([np.min(jw), np.max(jw)])
    plt.show()

    sim.plot_dc_graph()

    return None,False,np.nan


