import sys, os, warnings

import numpy as np
import sympy as sym

class NumericAnalyzer (object):
    def __init__ (self, gnd='gnd'):
        self.gnd = gnd
        self.devices = [ 'R', 'G', 'L', 'C', 'I', 'V', 'VCCS' ]
        self.components = {}

    def add (self, component, nodes, name, value):
        if component not in self.devices:
            print(f"Component has to be one of {self.devices}")
            return

        self.components[name] = { 'device': component
                                , 'value': value
                                , 'nodes': nodes }
        return self.components

    def analyze (self, jw=0.0):
        if self.gnd not in sum([cv['nodes'] for cv in self.components.values()], []) \
        or len(self.components) <= 0:
            warnings.warn(f"There is no {self.gnd} in this circuit:\n", RuntimeWarning)
            return None, None

        n_idx = {}
        i_idx = {}

        for name,component in self.components.items():
            val = component['value']

            for n in component['nodes']:
                if n not in n_idx.keys() and n != self.gnd:
                    n_idx[n] = len(n_idx)
            if component['device'] in ['V', 'I', 'VCCS'] and name not in i_idx.keys():
                i_idx[name] = len(i_idx)

            if component['device'] in ['G','R','C', 'L']:
                if component['device'] == 'R':
                    e = (1 / val)
                elif component['device'] == 'C':
                    e = (jw * val)
                elif component['device'] == 'L':
                    e = (1 / (jw * val))
                else:
                    e = val
                component['A'] = [ { 'a': n_idx[a]
                                   , 'b': n_idx[b]
                                   , 'e': e if a == b else -e }
                                   for a,b in [ (a,b) for a in component['nodes']
                                                      for b in component['nodes'] ]
                                   if a != self.gnd and b != self.gnd ]
            elif component['device'] == 'I':
                component['u'] = [ { 'a': n_idx[a]
                                   , 'e': e }
                                   for a,e in zip(component['nodes'], [-val, val])
                                   if a != self.gnd ]
            elif component['device'] == 'V':
                component['B'] = [ { 'a': n_idx[a]
                                   , 'i': i_idx[name]
                                   , 'e': e }
                                   for a,e in zip(component['nodes'], [1, -1])
                                   if a != self.gnd ]
                component['C'] = component['B']
                component['v'] = [ { 'i': i_idx[name]
                                   , 'e': val } ]
            elif component['device'] == 'VCCS':
                component['B'] = [ { 'a': n_idx[a]
                                   , 'i': i_idx[name]
                                   , 'e': e }
                                   for a,e in zip(component['nodes'][-2:], [1, -1])
                                   if a != self.gnd ]
                component['C'] = [ { 'a': n_idx[a]
                                   , 'i': i_idx[name]
                                   , 'e': e }
                                   for a,e in zip(component['nodes'][:2], [-1, 1])
                                   if a != self.gnd ]
                component['D'] = [ { 'i': i_idx[name]
                                   , 'j': i_idx[name]
                                   , 'e': (1 / val) } ]
            else:
                warnings.warn(f"Unknown Component {component}.", RuntimeWarning)
                return None, None

        m = len(n_idx)
        n = len(i_idx)

        A = np.zeros([m, m])
        B = np.zeros([m, n])
        C = np.zeros([n, m])
        D = np.zeros([n, n])
        u = np.zeros([m, 1])
        v = np.zeros([n, 1])

        for name,component in self.components.items():
            if 'A' in component.keys():
                for xpr in component['A']:
                    a,b,e = xpr['a'], xpr['b'], xpr['e']
                    A[a,b] = A[a,b] + e
            if 'B' in component.keys():
                for xpr in component['B']:
                    a,i,e = xpr['a'], xpr['i'], xpr['e']
                    B[a,i] = B[a,i] + e
            if 'C' in component.keys():
                for xpr in component['C']:
                    a,i,e = xpr['a'], xpr['i'], xpr['e']
                    C[i,a] = C[i,a] + e
            if 'D' in component.keys():
                for xpr in component['D']:
                    i,j,e = xpr['i'], xpr['j'], xpr['e']
                    D[i,j] = D[i,j] + e
            if 'u' in component.keys():
                for xpr in component['u']:
                    a,e = xpr['a'], xpr['e']
                    u[a] = u[a] + e
            if 'v' in component.keys():
                for xpr in component['v']:
                    i,e = xpr['i'], xpr['e']
                    v[i] = v[i] + e

        M = np.vstack(( np.hstack((A, B))
                      , np.hstack((C, D)) ))

        y = np.vstack(( u, v ))

        try:
            x = np.linalg.solve(M, y)
        except:
            print(f"Matrix is singular, approximating with lstsq")
            x = np.linalg.lstsq(M, y, rcond=None)[0]
            print(f"X: {x}")
            pass

        V = { f"V_{list(filter(lambda v: n_idx[v] == idx, n_idx))[0]}" : xpr
              for idx,xpr in enumerate(x[:m]) }

        I = { f"i_{list(filter(lambda i: i_idx[i] == idx, i_idx))[0]}" : xpr
              for idx,xpr in enumerate(x[-n:]) }

        return {k: v[0] for k,v in {**V, **I}.items()}

class SymbolicAnalyzer (object):
    def __init__ (self, gnd='gnd'):
        self.gnd = gnd
        self.devices = [ 'R', 'G', 'L', 'C', 'I', 'V', 'VCCS' ]
        self.components = {}

    def add (self, component, nodes, name):
        if component not in self.devices:
            print(f"Component has to be one of {self.devices}")
            return

        self.components[name] = { 'device': component
                                , 'nodes': nodes }
        return self.components

    def analyze (self):
        if self.gnd not in sum([cv['nodes'] for cv in self.components.values()], []) \
        or len(self.components) <= 0:
            warnings.warn(f"There is no {self.gnd} in this circuit:\n", RuntimeWarning)
            return None, None


        sym.var(list(self.components.keys()))
        s = sym.Symbol('s', imaginary=True)

        n_idx = {}
        i_idx = {}

        for name,component in self.components.items():
            for n in component['nodes']:
                if n not in n_idx.keys() and n != self.gnd:
                    n_idx[n] = len(n_idx)
            if component['device'] in ['V', 'I', 'VCCS'] and name not in i_idx.keys():
                i_idx[name] = len(i_idx)

            if component['device'] in ['G','R','C', 'L']:
                if component['device'] == 'R':
                    e = f"1 / {name}"
                elif component['device'] == 'C':
                    e = f"s * {name}"
                elif component['device'] == 'L':
                    e = f"1 / (s * {name})"
                else:
                    e = f"{name}"
                component['A'] = [ { 'a': n_idx[a]
                                   , 'b': n_idx[b]
                                   , 'e': ('+' if a == b else '-') + e }
                                   for a,b in [ (a,b) for a in component['nodes']
                                                      for b in component['nodes'] ]
                                   if a != self.gnd and b != self.gnd ]
            elif component['device'] == 'I':
                component['u'] = [ { 'a': n_idx[a]
                                   , 'e': e }
                                   for a,e in zip(component['nodes'], ['-' + name, name])
                                   if a != self.gnd ]
            elif component['device'] == 'V':
                component['B'] = [ { 'a': n_idx[a]
                                   , 'i': i_idx[name]
                                   , 'e': e }
                                   for a,e in zip(component['nodes'], ['1', '-1'])
                                   if a != self.gnd ]
                component['C'] = component['B']
                component['v'] = [ { 'i': i_idx[name]
                                   , 'e': name } ]
            elif component['device'] == 'VCCS':
                component['B'] = [ { 'a': n_idx[a]
                                   , 'i': i_idx[name]
                                   , 'e': e }
                                   for a,e in zip(component['nodes'][-2:], ['1', '-1'])
                                   if a != self.gnd ]
                component['C'] = [ { 'a': n_idx[a]
                                   , 'i': i_idx[name]
                                   , 'e': e }
                                   for a,e in zip(component['nodes'][:2], ['-1', '1'])
                                   if a != self.gnd ]
                component['D'] = [ { 'i': i_idx[name]
                                   , 'j': i_idx[name]
                                   , 'e': f"1 / {name}" } ]
            else:
                warnings.warn(f"Unknown Component {component}.", RuntimeWarning)
                return None, None

        m = len(n_idx)
        n = len(i_idx)

        A = sym.zeros(m, m)
        B = sym.zeros(m, n)
        C = sym.zeros(n, m)
        D = sym.zeros(n, n)
        u = sym.zeros(m, 1)
        v = sym.zeros(n, 1)

        for name,component in self.components.items():
            if 'A' in component.keys():
                for xpr in component['A']:
                    a,b,e = xpr['a'], xpr['b'], xpr['e']
                    A[a,b] = A[a,b] + sym.parse_expr(e)
            if 'B' in component.keys():
                for xpr in component['B']:
                    a,i,e = xpr['a'], xpr['i'], xpr['e']
                    B[a,i] = B[a,i] + sym.parse_expr(e)
            if 'C' in component.keys():
                for xpr in component['C']:
                    a,i,e = xpr['a'], xpr['i'], xpr['e']
                    C[i,a] = C[i,a] + sym.parse_expr(e)
            if 'D' in component.keys():
                for xpr in component['D']:
                    i,j,e = xpr['i'], xpr['j'], xpr['e']
                    D[i,j] = D[i,j] + sym.parse_expr(e)
            if 'u' in component.keys():
                for xpr in component['u']:
                    a,e = xpr['a'], xpr['e']
                    u[a] = u[a] + sym.parse_expr(e)
            if 'v' in component.keys():
                for xpr in component['v']:
                    i,e = xpr['i'], xpr['e']
                    v[i] = v[i] + sym.parse_expr(e)

        M = sym.Matrix.vstack( sym.Matrix.hstack(sym.simplify(A), sym.simplify(B))
                             , sym.Matrix.hstack(sym.simplify(C), sym.simplify(D)) )

        y = sym.Matrix.vstack( u, v )
        #y = sym.Matrix.hstack( u, v )

        x = M.LUsolve(y)

        V = { f"V_{list(filter(lambda v: n_idx[v] == idx, n_idx))[0]}" : xpr
              for idx,xpr in enumerate(x[:m]) }

        I = { f"i_{list(filter(lambda i: i_idx[i] == idx, i_idx))[0]}" : xpr
              for idx,xpr in enumerate(x[-n:]) }

        return V, I
