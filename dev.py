import time

import numpy as np
import pandas as pd

import joblib
from sklearn import preprocessing

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data.dataset import TensorDataset
from torch.utils.data.dataloader import DataLoader

class PrimitiveDevice (object):
    def __init__ (self, prefix, typ='n'):
        self.dev_type   = typ
        self.name_ipps  = [ 'P_l', 'V_D', 'V_G', 'V_B' ]
        self.name_opps  = [ 'OPP_id', 'OPP_fug'
                          , 'OPP_vth', 'OPP_vdsat'
                          , 'OPP_gm', 'OPP_gds', 'OPP_gmb'
                          , 'OPP_cgd', 'OPP_cgs', 'OPP_cgb'
                          , 'OPP_cds', 'OPP_cdb', 'OPP_csb' ]

        self.opps       = [ 'OPP_id', 'OPP_rout', 'OPP_ron'
                           , 'OPP_gm', 'OPP_gds', 'OPP_gmb' ]
        self.oppc       = [ 'OPP_cgd', 'OPP_cgb', 'OPP_cgs'
                           , 'OPP_cds', 'OPP_csb', 'OPP_cdb' ]
        self.oppf       = [ 'OPP_fug', 'OPP_vth', 'OPP_vdsat', 'OPP_vearly' ]

        #self.opp_idx    = [i for i,e in enumerate(self.name_opps)
        #                             if e not in [ 'OPP_vth', 'OPP_vearly' ]]
        self.opp_idx    = list(range(len(self.name_opps)))

        self.num_ipps   = len(self.name_ipps)
        self.num_opps   = len(self.name_opps)

        self.device     = torch.device('cuda') if torch.cuda.is_available()\
                                               else torch.device('cpu')
        self.in_trafo   = joblib.load(prefix + '.input')
        self.out_trafo  = joblib.load(prefix + '.output')
        self.model      = PredictNet(self.num_ipps, self.num_opps)
        self.model.to(self.device)
        self.model.load_state_dict(torch.load( prefix + '.model'
                                             , map_location=self.device ))
        self.model.eval()

    def predict (self, w, l, vd, vg, vs, vb, batch_size=1):
        vd_             = vd - vs
        vg_             = vg - vs
        vb_             = vb - vs

        input_arr       = np.array([l, vd_, vg_, vb_])
        input_data      = input_arr.T if batch_size > 1 \
                                      else input_arr.reshape(1,-1)

        input_tf        = self.in_trafo.transform(input_data)
        input_set       = TensorDataset(torch.Tensor(input_tf))
        input_loader    = DataLoader(input_set, batch_size)

        output_array    = None
        with torch.no_grad():
            for batch in input_loader:
                output_model    = self.model(batch[0].to(self.device))
                output_np       = output_model.to("cpu").data.numpy()
                output_tf       = self.out_trafo.inverse_transform(output_np)
                output_array    = output_tf if output_array is None \
                                            else np.concatenate( ( output_array
                                                                 , output_tf )
                                                               , axis=0 )

        output_data     = output_array.T if batch_size > 1 \
                                         else output_array[0]

        prediction      = { opp.replace('OPP_', '') : (prd * w) if opp in self.oppc
                                                                or opp in self.opps
                                                                else prd
                            for opp,prd in zip( [self.name_opps[idx] for idx in self.opp_idx]
                                              , output_data[self.opp_idx] ) }

        return prediction

class PredictNet (nn.Module):
    def __init__ (self, n_i, n_o):
        super(PredictNet, self).__init__()
        self.fc_1   = nn.Linear(n_i, 128)
        self.fc_2   = nn.Linear(128, 256)
        self.fc_3   = nn.Linear(256, 512)
        self.fc_4   = nn.Linear(512, 1024)
        self.fc_5   = nn.Linear(1024, 512)
        self.fc_6   = nn.Linear(512, 256)
        self.fc_7   = nn.Linear(256, 128)
        self.fc_8   = nn.Linear(128, n_o)

    def forward (self, x):
        x   = F.relu(self.fc_1(x))
        x   = F.relu(self.fc_2(x))
        x   = F.relu(self.fc_3(x))
        x   = F.relu(self.fc_4(x))
        x   = F.relu(self.fc_5(x))
        x   = F.relu(self.fc_6(x))
        x   = F.relu(self.fc_7(x))
        out = self.fc_8(x)
        return out
