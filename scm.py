import sys, os, warnings

import numpy as np
import sympy as sym

from datetime import datetime
from functools import partial

from dev import PrimitiveDevice

class Schematic (object):
    """
    doc test blblblbl
    """
    def __init__ (self, gnd='gnd'):
        self.gnd = gnd

        self.components = { 'R': 0
                          , 'G': 0
                          , 'C': 0
                          , 'V': 0
                          , 'I': 0
                          , 'NM': 0
                          , 'PM': 0 }
        self.devices = {}

        self.simulate = lambda: self.sim.simulate(self.devices)

    def add_g (self, G, p, m, name=None, parent=None):
        name = name if name is not None else 'G' + str(self.components['G'])
        self.devices[name] = { 'type': 'passive'
                             , 'component': 'G'
                             , 'value': G
                             , 'pins': { '+': p
                                       , '-': m }
                             , 'unit': 'S'
                             , 'idc': lambda φp,φm: (φp - φm) * G
                             , 'parent': parent }

        self.components['G'] =+ 1
        return self.devices[name]

    def add_r (self, R, p, m, name=None, parent=None):
        name = name if name is not None else 'R' + str(self.components['R'])
        self.devices[name] = { 'type': 'passive'
                             , 'component': 'R'
                             , 'value': R
                             , 'pins': { '+': p
                                       , '-': m }
                             , 'unit': 'Ω'
                             , 'idc': lambda φp,φm: (φp - φm) / R
                             , 'parent': parent }

        self.components['R'] =+ 1
        return self.devices[name]

    def add_c (self, C, p, m, name=None, parent=None):
        name = name if name is not None else 'C' + str(self.components['C'])
        self.devices[name] = { 'type': 'passive'
                             , 'component': 'C'
                             , 'value': C
                             , 'pins': { '+': p
                                       , '-': m }
                             , 'unit': 'F'
                             , 'idc': lambda φp,φm: 0
                             , 'parent': parent }

        self.components['C'] =+ 1
        return self.devices[name]

    def add_v (self, dcv, p, m, acm=None, acp=None, name=None, parent=None):
        name = name if name is not None else 'V' + str(self.components['V'])
        self.devices[name] = { 'type': 'source'
                             , 'component': 'V'
                             , 'value': dcv
                             , 'acm': acm
                             , 'acp': acp
                             , 'pins': { '+': p
                                       , '-': m }
                             , 'unit': 'V'
                             , 'idc': lambda φp,φm: 0
                             , 'parent': parent }

        self.components['V'] =+ 1
        return self.devices[name]

    def add_vcvs (self, ref, p, m, name=None, parent=None):
        name = name if name is not None else 'V' + str(self.components['V'])
        self.devices[name] = { 'type': 'source'
                             , 'component': 'VCVS'
                             , 'value': 0.0
                             , 'controller': ref
                             , 'pins': { '+': p
                                       , '-': m }
                             , 'unit': 'V'
                             , 'idc': lambda φp,φm: 0
                             , 'parent': parent }

        self.components['V'] =+ 1
        return self.devices[name]

    def add_i (self, dci, p, m, amp=None, frq=None, name=None, parent=None):
        name = name if name is not None else 'I' + str(self.components['I'])
        self.devices[name] = { 'type': 'source dc'
                             , 'component': 'I'
                             , 'value': dci
                             , 'frq': frq
                             , 'amp': amp
                             , 'pins': { '+': p
                                       , '-': m }
                             , 'idc': lambda φp,φm: dci
                             , 'iac': lambda φp,φm: aci
                             , 'unit': 'A'
                             , 'parent': parent }

        self.components['I'] =+ 1
        return self.devices[name]

    def add_nmos_wl ( self, g, d, s, b, w, l, model_prefix
                    , name=None, parent=None ):
        model = PrimitiveDevice(model_prefix)
        name = name if name is not None else 'NM' + str(self.components['NM'])

        self.devices[name] = { 'type': 'active'
                             , 'component': 'nmos'
                             , 'w': w
                             , 'l': l
                             , 'pins': { 'D': d
                                       , 'G': g
                                       , 'S': s
                                       , 'B': b }
                             , 'opps': model.predict
                             , 'parent': None }

        self.components['NM'] =+ 1
        return self.devices[name]

    def add_pmos_wl ( self, g, d, s, b, w, l, model_prefix
                    , name=None, parent=None):
        model = PrimitiveDevice(model_prefix)
        name = name if name is not None else 'PM' + str(self.components['PM'])

        self.devices[name] = { 'type': 'active'
                             , 'component': 'pmos'
                             , 'w': w
                             , 'l': l
                             , 'pins': { 'D': d
                                       , 'G': g
                                       , 'S': s
                                       , 'B': b }
                             , 'opps': model.predict
                             , 'parent': None }

        self.components['PM'] =+ 1
        return self.devices[name]

    def add_nmos_gm (self, g, d, s, b, g_m, i_d, f_ug, name=None):
        try:
            None
        except NotImplementedError:
            print("This function is not yet Implemented, be patient.")
        return None

    def add_pmos_gm (self, g, d, s, b, g_m, i_d, f_ug, name=None):
        try:
            None
        except NotImplementedError:
            print("This function is not yet Implemented, be patient.")
        return None

    def rem (self, name):
        if name in self.devices.keys():
            del self.devices[name]
            return True
        else:
            warnings.warn( f"No component by the name {name} in this Circuit."
                         , RuntimeWarning)
            return False
        return True

    def small_signal_circuit (self):
        if len(self.devices) == 0:
            print(f"Can't analyze an empty circuit.\n")
            return None
        return
