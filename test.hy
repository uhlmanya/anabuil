(import [sim [Simulator]] 
        [mna [NumericAnalyzer]] 
        [scm [Schematic]]
        [dev [PrimitiveDevice]])
(import mod)

(import [numpy :as np])
(import [pandas :as pd])
(import [matplotlib :as mpl])
(import [matplotlib.pyplot :as plt])

(require [hy.contrib.walk [let]])
(require [hy.contrib.loop [loop]])

(setv nmos1v (PrimitiveDevice "./models/gpdk090_nmos1v/nmos1v"))

; (w, l, vd, vg, vs, vb)
(setv w 3.25e-6)
(setv l 300e-9)
(setv vd 0.6)
(setv vs 0.0)
(setv vb 0.0)

(setv vgs (.linspace np 0 1.2 1000))
(setv ops (lfor vg vgs 
                (.predict nmos1v w l vd vg vs vb)))

(.plot plt vgs (lfor op ops (get op "id")))
(.plot plt vgs (lfor op ops (get op "gds")))

(.show plt)

(setv vg 0.4)
(setv ws (.linspace np 300e-9 30e-6))
(setv ops (lfor w ws 
                (.predict nmos1v w l vd vg vs vb)))

(setv ids (lfor op ops (get op "id")))
(setv gds (lfor op ops (get op "gds")))
(setv i (* vd (.array np gds)))
(.plot plt ws ids)
(.plot plt ws gds)
(.plot plt ws i)

(.show plt)

;; Module Data Generation
(setv scm (.CommonSource mod 3.25e-6 300e-9 0.3 0.05 1e-12 20e-6))
(setv sim (Simulator :optimizer "Nelder-Mead" :abstol 1e-13 :gnd "SS"))

(setv op (.dc-operating-point sim scm.devices))
(setv analyzer (.nodal-analyzer sim scm.devices op))

(setv res (analyzer :jw 0))
(setv gain (* 20 (.log10 np (.abs np (/ (get res "V_O") (get res "V_I"))))))

(setv jw (.linspace np 0 1e9 :num (int 1e5)))
(setv vo (.array np (lfor s jw (get (analyzer :jw s) "V_O"))))
(setv vi (.array np (lfor s jw (get (analyzer :jw s) "V_I"))))

(setv Av (* 20 (.log10 np (.abs np (/ vo vi)))))
(setv f3dB (get jw (.argmax np (<= Av (- gain 3)))))
(setv UGBW (get jw (.argmax np (<= Av 0))))

(.semilogx plt jw Av)
(.axhline plt :y gain :color "b" :linestyle "dashed" :linewidth 1)
(.axvline plt :x f3dB :color "r" :linestyle "dashed" :linewidth 1)
(.axhline plt :y (- gain 3) :color "r" :linestyle "dashed" :linewidth 1)
(.axvline plt :x UGBW :color "g" :linestyle "dashed" :linewidth 1)
(.axhline plt :y 0 :color "g" :linestyle "dashed" :linewidth 1)
(.grid plt)
(.xlim plt [(.min np jw) (.max np jw)])
(.show plt)

(.plot-dc-graph sim)

