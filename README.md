# ANABUIL -- Analog Building Blocks

Machine Learning powered Basic Building Blocks
for Analog Integrated Circuit Design.

## Building Blocks

There will be building blocks for the most common and
most reusable basic circuits of analog design. All of these
blocks map the most common design parameters directly to
corresponding $`\frac{W}{L}`$ for use with Layout Generators.

### Current Source

The _Current Source_ block maps

```math
\begin{bmatrix}
i_{d}\\ g_{ds}\\ V_{in}
\end{bmatrix}
\Rightarrow
\begin{bmatrix}
    W\\L
\end{bmatrix}
```

With this, a designer can choose a drain current, drain-source resistance
and a gate voltage, and immediately get the corresponding $`\frac{W}{L}`$.

### Current Mirror

The _Current Mirror_ block maps:

```math
\begin{bmatrix}
    I_{in}\\ I_{out}\\ R_{in}\\ R_{out}\\ V_{dsat}\\ V_{OVD}
\end{bmatrix}
\Rightarrow
\begin{bmatrix}
    W_{1}\\ W_{2}\\ L
\end{bmatrix}
```

The mirror ratio is implicitly given by specifying both dc currents $`I_{in}`$
and $`I_{out}`$. Based on this the trained model can reproduce the 
$`\frac{W}{L}`$ for the corresponding operating point.

### Differential Pair

The _Differential Pair_ block maps:

```math
\begin{bmatrix}
    g_{ds}\\ f_{-3dB}\\ I_{ref}
\end{bmatrix}
\Rightarrow
\begin{bmatrix}
    W \\ L
\end{bmatrix}
```

There will be only one $`\frach{W}{L}`$ as output of this block, because
a miss-matched differential pair is undesirable.
