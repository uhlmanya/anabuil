(import os datetime time)
(import [pathlib [Path]])
(import [pandas :as pd])
(import [numpy :as np])
(import [scipy :as sp])
(import [matplotlib :as mpl])
(import [matplotlib.pyplot :as plt])
(import [matplotlib [cm]])
(import [mpl_toolkits.mplot3d [axes3d]])
(import joblib)
(import [sklearn.preprocessing [QuantileTransformer]])
(import [sklearn.model_selection._split [train_test_split]])
(import torch)
(import [torch.nn :as nn])
(import [torch.nn.functional :as func])
(import [torch.optim :as optim])
(import [torch.utils.data.dataset [TensorDataset]])
(import [torch.utils.data.dataloader [DataLoader]])

(require [hy.contrib.walk [let]])
(require [hy.contrib.loop [loop]])

(setv eng (.EngFormatter mpl.ticker))

; Neural Network Class
(defclass mod-net [nn.Module]
    (defn --init-- [self num-ips num-ops]
        (.--init-- (super mod-net self))
        (setv self.fc-1 (.Linear nn num-ips 128))
        (setv self.fc-2 (.Linear nn 128 256))
        (setv self.fc-3 (.Linear nn 256 512))
        (setv self.fc-4 (.Linear nn 512 1024))
        (setv self.fc-5 (.Linear nn 1024 512))
        (setv self.fc-6 (.Linear nn 512 256))
        (setv self.fc-7 (.Linear nn 256 128))
        (setv self.fc-8 (.Linear nn 128 num-ops)))
    (defn forward [self x]
        (->> x (.fc-1 self) (.relu func)
               (.fc-2 self) (.relu func)
               (.fc-3 self) (.relu func) 
               (.fc-4 self) (.relu func)
               (.fc-5 self) (.relu func)
               (.fc-6 self) (.relu func)
               (.fc-7 self) (.relu func)
               (.fc-8 self) (.relu func))))

;; CUDA setup
(setv device (.device torch (if (.is-available torch.cuda) "cuda" "cpu")))
(print f"We're running on a {(.get-device-name torch.cuda)}")

;; File System Setup
(setv module-name "nmos1v-ccs")
(setv module-db (+ "./data/" module-name "/" module-name ".h5"))
(setv module-dir (+ "./data/" module-name "/"
                (.strftime (.now datetime.datetime) "%Y%m%d-%H%M%S")))
(setv prefix (+ module-dir "/" module-name))
(.mkdir (Path module-dir) :parents True :exist-ok True)

;; Read Data Frame
(setv module-df (as-> module-db df
                      (.read-hdf pd df "database")
                      (get df (.all (.isfinite np df) 1))
                      (.dropna df)))

;; Define inputs and outputs
; Common Source Amplifier
;(setv input-cols ["A0dB" "fug" "f3dB" "Vin" "CL" "Iref"])
;(setv output-cols ["w" "l" "VO" "vo" "id"])

; Differential Pair 
;(setv input-cols ["A0dB" "fug" "f3dB" "Vincm" "Vind" "Inp" "Inn" "CL" "Iref"])
;(setv output-cols ["wp" "lp" "wn" "ln" "VOn" "von" "VOp" "vop"])

; Current Mirror 
;(setv input-cols ["Iref" "M" "Rout" "Rin" "Vmin"])
;(setv output-cols ["wi" "li" "wo" "lo" "VO" "vo"])

; Constant Current Source
(setv input-cols ["id" "gds" "vgs"])
(setv output-cols ["w" "l"])

;; Normalize Data
(setv input-raw (. (get module-df input-cols) values))
(setv output-raw (. (get module-df output-cols) values))

(setv input-transformer (QuantileTransformer :output-distribution "uniform" 
                                             :random-state 42))
(setv output-transformer (QuantileTransformer :output-distribution "uniform" 
                                              :random-state 42))

(setv input-data (.fit-transform input-transformer input-raw))
(setv output-data (.fit-transform output-transformer output-raw))

(.dump joblib input-transformer (+ prefix ".input"))
(.dump joblib output-transformer (+ prefix ".output"))

;; Setup Training Data
(setv batch-size 100)
(setv test-split 0.2)

(setv (, X-train
         X-valid
         Y-train
         Y-valid) (train-test-split input-data
                                    output-data
                                    :test-size test-split
                                    :shuffle True
                                    :random-state 42))

(setv train-set (TensorDataset (.Tensor torch X-train) (.Tensor torch Y-train)))
(setv valid-set (TensorDataset (.Tensor torch X-valid) (.Tensor torch Y-valid)))

(setv train-loader (DataLoader train-set batch-size :pin-memory True))
(setv valid-loader (DataLoader valid-set batch-size :pin-memory True))

;; Define network
(setv module-net (.to (mod-net (len input-cols) 
                               (len output-cols)) 
                 device))

(setv loss-mse (.MSELoss nn))
(setv loss-mae (.L1Loss nn))

(setv optim-adamw (.AdamW optim (.parameters module-net)))
(setv optim-adam (.Adam optim (.parameters module-net)))

;; Training Process
(defn forward-train [X-train Y-train &optional [optim optim-adam]]
    (let [_ (.zero-grad optim)
          out-train (module-net X-train)
          mse (loss-mse out-train Y-train)
          mae (loss-mae out-train Y-train)]
        (.backward mse)
        (.step optim)
        (, (.item mse) (.item mae))))

(defn forward-valid [X-valid Y-valid]
    (let [out-valid (module-net X-valid)
          mse (loss-mse out-valid Y-valid)
          mae (loss-mae out-valid Y-valid)]
        (, (.item mse) (.item mae))))

(defn epoch-step [epoch-count]
    (let [t0 (.replace (.now datetime.datetime) :microsecond 0)
          _ (.train module-net)
          train-err (reduce (fn [acc err] 
                                (tuple (lfor x (zip acc err) (sum x))))
                            (lfor (, X-train Y-train) train-loader
                                  (forward-train (.to X-train device)
                                                 (.to Y-train device)))
                            (, 0 0))
          valid-err (with [(.no-grad torch)]
                        (.eval module-net)
                        (reduce (fn [acc err] 
                                    (tuple (lfor x (zip acc err) (sum x))))
                                (lfor (, X-valid Y-valid) valid-loader
                                      (forward-valid (.to X-valid device)
                                                     (.to Y-valid device)))))
          train-mse-avg (/ (first train-err) (len train-loader))
          train-mae-avg (/ (second train-err) (len train-loader))
          valid-mse-avg (/ (first valid-err) (len valid-loader))
          valid-mae-avg (/ (second valid-err) (len valid-loader))
          t1 (.replace (.now datetime.datetime) :microsecond 0)
          epc-str (.format "[{0} - {1}] Epoch {2}/{3}: " t0 t1 (inc epoch-count) num-epochs)
          mse-str (.format "Training MSE: {0}" (eng train-mse-avg))
          mae-str (.format "Validation MAE: {0}" (eng valid-mae-avg))]
        (print (+ epc-str "\n\t" mse-str "\n\t" mae-str))
        (, train-mse-avg valid-mae-avg)))

(setv num-epochs 100)

(setv (, train-losses valid-losses)
    (loop [[epoch 0] 
           [best-candidate None] 
           [train-losses (list)] 
           [valid-losses (list)]]
        (let [error (epoch-step epoch)
              train-mse (first error)
              valid-mae (second error)
              candidate (if (or (is best-candidate None) 
                                (> best-candidate valid-mae))
                            valid-mae
                            best-candidate)]
            (when (not (= candidate best-candidate))
                (.save torch (.state-dict module-net) (+ prefix ".model"))
                (print (.format "\tNew Candidate Net saved!")))
            (if (< epoch num-epochs)
                (recur (inc epoch) 
                       candidate
                       (+ train-losses [train-mse])
                       (+ valid-losses [valid-mae]))
                (, train-losses valid-losses)))))

; Plot and evaluate Progress
(setv fig (.figure plt :figsize (.figaspect plt 0.4)))

(setv ax-train (.add-subplot fig 1 2 1))
(.plot ax-train (range num-epochs) train-losses)
(.set-xlabel ax-train "#Epoch")
(.set-ylabel ax-train "Training MSE")
(.set-title ax-train "Training Loss")

(setv ax-valid (.add-subplot fig 1 2 2))
(.plot ax-valid (range num-epochs) valid-losses)
(.set-xlabel ax-valid "#Epoch")
(.set-ylabel ax-valid "Validation MAE")
(.set-title ax-valid "Validation Loss")

(.show plt)
