import scipy as sp
from scipy.optimize import minimize
import numpy as np
import sympy as sym
import networkx as nx
import matplotlib as mpl
import matplotlib.pyplot as plt

import time
import numbers

from mna import NumericAnalyzer, SymbolicAnalyzer
from sim import Simulator
from scm import Schematic

formatter = mpl.ticker.EngFormatter()

def  test_diffamp ():
    NM = "./models/gpdk090_nmos1v/nmos1v"
    PM = "./models/gpdk090_pmos1v/pmos1v"

    da = Schematic()

    da.add_v(1.2, "DD", "gnd", name="Vdd")
    da.add_v(0.6, "B", "gnd", name="Vbias")
    da.add_v(0.3, "Ip", "gnd", acm=5e-3, name="Vip")
    da.add_v(0.3, "In", "gnd", acm=5e-3, name="Vin")

    da.add_c(1e-12, "O", "gnd", name="CL")
    da.add_nmos_wl("B", "W", "gnd", "gnd", 3.25e-6, 3e-7, NM, name='NM1')
    da.add_nmos_wl("In", "X", "W", "W", 3.25e-6, 3e-7, NM, name='PM21')
    da.add_nmos_wl("In", "Y", "W", "W", 3.25e-6, 3e-7, NM, name='PM22')
    da.add_pmos_wl("X", "X", "DD", "DD", 3.25e-6, 3e-7, PM, name='NM31')
    da.add_pmos_wl("Y", "Y", "DD", "DD", 3.25e-6, 3e-7, PM, name='NM32')
    da.add_pmos_wl("X", "Z", "DD", "DD", 3.25e-6, 3e-7, PM, name='NM41')
    da.add_pmos_wl("Y", "O", "DD", "DD", 3.25e-6, 3e-7, PM, name='NM42')
    da.add_pmos_wl("Z", "gnd", "Z", "Z", 3.25e-6, 3e-7, PM, name='NM51')
    da.add_pmos_wl("Z", "gnd", "O", "O", 3.25e-6, 3e-7, PM, name='NM52')

    sim = Simulator(optimizer='Nelder-Mead', abstol=1e-13)

    t0 = time.time()
    op = sim.dc_operating_point(da.devices)
    t1 = time.time()
    print(f"Found DC Operating Point in {formatter(t1 - t0)}s")

    analyzer = sim.nodal_analyzer(da.devices, op)
    res = analyzer(jw=0)
    gain = res['V_O'] / res['V_Ip']

    jw = np.linspace(0, 1e10, num=int(1e3))
    vo = np.array([ analyzer(jw=s)['V_O'] for s in jw ])
    vi = np.array([ analyzer(jw=s)['V_Ip'] for s in jw ])
    Av = vo/vi

    f3dB = jw[np.argmax(Av <= (gain - 3))]
    UGBW = jw[np.argmax(Av <= 0)]

    sim.plot_dc_graph()

    return da

def test_csc ():
    csc = Schematic()

    csc.add_v(1.2, "DD", "gnd", name="Vdd")
    csc.add_v(0.3, "I", "gnd", acm=5e-3, name="Vin")
    csc.add_v(1.0, "B", "gnd", name="Vbias")
    csc.add_r(1e4, "DD", "O", name="RL")
    csc.add_c(5e-15, "O", "gnd", name="CL")
    csc.add_nmos_wl( "I", "X", "gnd", "gnd", 3.25e-6, 3e-7
                  , "./models/gpdk090_nmos1v/nmos1v" )
    csc.add_nmos_wl( "B", "O", "X", "gnd", 5e-6, 3e-7
                  , "./models/gpdk090_nmos1v/nmos1v" )

    sim = Simulator(optimizer='Nelder-Mead', abstol=1e-13)
    res = sim.dc_operating_point(csc.devices)
    analyzer = sim.nodal_analyzer(csc.devices, res)

    gain = 20 * np.log10(np.abs(analyzer(jw=0)['V_O'] / analyzer(jw=0)['V_I']))

    sim.plot_dc_graph()

    jw = np.linspace(0, 1e10, num=int(1e4))
    A = lambda s : 20 * np.log10(np.abs( analyzer(jw=s)['V_O'] \
                                       / analyzer(jw=s)['V_I'] ))
    Av = np.array([ A(s) for s in jw ])

    f3dB = jw[np.argmax(Av <= (gain - 3))]
    UGBW = jw[np.argmax(Av <= 0)]

    plt.semilogx(jw, Av)
    plt.axhline(y=gain, color='b', linestyle='dashed', linewidth=1)
    plt.axvline(x=f3dB, color='r', linestyle='dashed', linewidth=1)
    plt.axhline(y=(gain - 3), color='r', linestyle='dashed', linewidth=1)
    plt.axvline(x=UGBW, color='g', linestyle='dashed', linewidth=1)
    plt.axhline(y=0, color='g', linestyle='dashed', linewidth=1)
    plt.grid()
    plt.show()

    return res

def test_cs ():
    cs = Schematic()
    cs.add_v(1.2, "DD", "gnd", name="Vdd")
    cs.add_v(0.4, "I", "gnd", acm=1.0, name="Vin")
    cs.add_r(3e3, "DD", "O", name="RL")
    cs.add_c(1e-15, "O", "gnd", name="CL")
    cs.add_nmos_wl( "I", "O", "gnd", "gnd", 2.5e-6, 3e-7
                  , "./models/gpdk090_nmos1v/nmos1v" )
    sim = Simulator()
    rs = sim.simulate(cs.devices)
    return rs

def test_simple ():
    circ = Analyzer(gnd='gnd!')
    circ.add('V', ['A', 'gnd!'], 'V1')
    circ.add('C', ['A', 'B'], 'C1')
    circ.add('R', ['B', 'gnd!'], 'R1')
    circ.analyze()
    return circ

def cs_ils():
    RL = sym.Symbol('RL')
    CL = sym.Symbol('CL')
    Csb0 = sym.Symbol('Csb0')
    Cdb0 = sym.Symbol('Cdb0')
    VDD = sym.Symbol('VDD')
    gmb0 = sym.Symbol('gmb0')
    gds0 = sym.Symbol('gds0')
    Cds0 = sym.Symbol('Cds0')
    Cgb0 = sym.Symbol('Cgb0')
    Cgs0 = sym.Symbol('Cgs0')
    VI = sym.Symbol('VI')
    gm0 = sym.Symbol('gm0')
    s = sym.Symbol('s')
    Cgd0 = sym.Symbol('Cgd0')

    A=sym.Matrix([[gds0 + 1/RL + s*CL + s*Cdb0 + s*Cds0 + s*Cgd0 ,                   - s*Cgd0                    ,                       0                      ],
                  [                  - s*Cgd0                    ,           s*Cgb0 + s*Cgd0 + s*Cgs0            ,                       0                      ],
                  [                      0                       ,                       0                       ,                       0                      ]])

    B=sym.Matrix([[0 , 0 , 1 , 1],
                  [0 , 1 , 0 , 0],
                  [1 , 0 , 0 , 0]])

    C=sym.Matrix([[ 0  ,  0  ,  1 ] ,
                  [ 0  ,  1  ,  0 ] ,
                  [ 0  , - 1 ,  0 ] ,
                  [ 0  ,  0  ,  0 ]])

    D=sym.Matrix([[  0    ,   0    ,   0    ,   0   ] ,
                  [  0    ,   0    ,   0    ,   0   ] ,
                  [  0    ,   0    , 1/gm0  ,   0   ] ,
                  [  0    ,   0    ,   0    , 1/gmb0]])

    M=sym.Matrix.vstack(sym.Matrix.hstack(A,B)
    , sym.Matrix.hstack(C,D))
    y=sym.Matrix([0, 0, 0, 0 + 1*VDD, 0 + 1*VI, 0, 0])

    x=M.LUsolve(y)
    V_O=x[0]
    V_I=x[1]
    V_DD=x[2]
    i_V_10=x[3]
    i_V_9=x[4]
    i_VCCS_8=x[5]
    i_VCCS_7=x[6]

def test_cascode ():
    circ = Analyzer(gnd='gnd!')
    circ.add("C", ["gnd!", "gnd!"], "Cgs1")
    circ.add("C", ["gnd!", "O"], "Cgd1")
    circ.add("C", ["gnd!", "gnd!"], "Cgb1")
    circ.add("C", ["O", "X"], "Cds1")
    circ.add("C", ["O", "gnd!"], "Cdb1")
    circ.add("C", ["X", "gnd!"], "Csb1")
    circ.add("G", ["O", "X"], "gds1")
    circ.add("VCCS", ["gnd!", "X", "O", "X"], "gmb1")
    circ.add("VCCS", ["gnd!", "X", "O", "X"], "gm1")

    circ.add("C", ["I", "gnd!"], "Cgs0")
    circ.add("C", ["I", "X"], "Cgd0")
    circ.add("C", ["I", "gnd!"], "Cgb0")
    circ.add("C", ["X", "gnd!"], "Cds0")
    circ.add("C", ["X", "gnd!"], "Cdb0")
    circ.add("C", ["gnd!" "gnd!"], "Csb0")
    circ.add("G", ["X", "gnd!"], "gds0")
    circ.add("VCCS", ["gnd!", "gnd!", "X", "gnd!"], "gmb0")
    circ.add("VCCS", ["I", "gnd!", "X", "gnd!"], "gm0")

    circ.add("V", ["I", "gnd!"], "VI")
    circ.add("V", ["DD", "gnd!"], "VDD")
    circ.add("R", ["DD", "O"], "RL")
    circ.add("C", ["O", "gnd!"], "CL")
    return circ.analyze()

def ils_cascode ():
    sym.var( ','.join( [ 'Cgd0', 'Cgs0', 'Cgb0', 'Cds0', 'Cdb0', 'Csb0'
                       , 'Cgd1', 'Cgs1', 'Cgb1', 'Cds1', 'Cdb1', 'Csb1'
                       , 'gm0', 'gmb0', 'gds0', 'gm1', 'gmb1', 'gds1'
                       , 'RL', 'CL', 'VI', 'VDD' ] ) )

    s = sym.Symbol('s', imaginary=True)

    A = sym.Matrix([[     gds1 + 1/RL + s*CL + s*Cdb1 + s*Cds1 + s*Cgd1       ,                     - gds1 - s*Cds1                      ,                            0                             ,                          - 1/RL                         ]
                   ,[                    - gds1 - s*Cds1                      , gds0 + gds1 + s*Cdb0 + s*Cds0 + s*Cgd0 + s*Csb1 + s*Cds1 ,                         - s*Cgd0                         ,                            0                            ]
                   ,[                           0                             ,                         - s*Cgd0                         ,                 s*Cgb0 + s*Cgd0 + s*Cgs0                 ,                            0                            ]
                   ,[                         - 1/RL                          ,                            0                             ,                            0                             ,                           1/RL                          ]])

    B = sym.Matrix([[ 0  ,  0  ,  0  ,  1  ,  1  ,  0 ]
                   ,[ 0  ,  0  ,  1  , - 1 , - 1 ,  1 ]
                   ,[ 1  ,  0  ,  0  ,  0  ,  0  ,  0 ]
                   ,[ 0  ,  1  ,  0  ,  0  ,  0  ,  0 ]])

    C = sym.Matrix([[ 0  ,  0  ,  1  ,  0 ]
                   ,[ 0  ,  0  ,  0  ,  1 ]
                   ,[ 0  ,  0  , - 1 ,  0 ]
                   ,[ 0  ,  1  ,  0  ,  0 ]
                   ,[ 0  ,  1  ,  0  ,  0 ]
                   ,[ 0  ,  0  ,  0  ,  0 ]])

    D = sym.Matrix([[  0    ,   0    ,   0    ,   0    ,   0    ,   0   ]
                   ,[  0    ,   0    ,   0    ,   0    ,   0    ,   0   ]
                   ,[  0    ,   0    , 1/gm0  ,   0    ,   0    ,   0   ]
                   ,[  0    ,   0    ,   0    , 1/gm1  ,   0    ,   0   ]
                   ,[  0    ,   0    ,   0    ,   0    , 1/gmb1 ,   0   ]
                   ,[  0    ,   0    ,   0    ,   0    ,   0    , 1/gmb0]])

    M = sym.Matrix.vstack( sym.Matrix.hstack(A,B)
                         , sym.Matrix.hstack(C,D) )

    y = sym.Matrix([0, 0, 0, 0, 0 + 1*VI, 0 + 1*VDD, 0, 0, 0, 0])

    x = M.LUsolve(y)

    X = { 'V_O' : x[0]
        , 'V_X' : x[1]
        , 'V_I' : x[2]
        , 'V_DD': x[3]
        , 'i_in': x[4]
        , 'i_dd': x[5]
        , 'i_d0': x[6]
        , 'i_d1': x[7]
        , 'i_b1': x[8]
        , 'i_b0': x[9] }
    return X

